from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.index, name='index'),
    path('add-matkul/', views.addmatkul, name='add-matkul'),
    path("delete-matkul/<int:id>",views.delete_matkul,name="delete-matkul"),
    path("/<int:id>",views.detail_matkul,name="detail-matkul")
]