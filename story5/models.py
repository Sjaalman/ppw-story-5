from django.db import models
from django.shortcuts import reverse

# Create your models here.
semester_choices = [
    ("2020/2021 Gasal","2020/2021 Gasal"),
    ("2020/2021 Genap","2020/2021 Genap"),
    ("2021/2022 Gasal","2021/2022 Gasal"),
    ("2021/2022 Genap","2021/2022 Genap"),
    ("2022/2023 Gasal","2022/2023 Gasal"),
    ("2022/2023 Genap","2022/2023 Genap")
]

class MatKul(models.Model):
    nama = models.TextField(max_length = 50)
    dosen = models.TextField(max_length = 50)
    sks = models.IntegerField()
    deskripsi = models.TextField(max_length = 500, default='')
    semester = models.TextField(choices = semester_choices)
    ruang = models.TextField(max_length = 50)

    def __str__(self):
        return "{} ({})".format(self.nama,self.dosen)
    
    def get_absolute_url(self):
        return reverse("story5:detail-matkul",args=[self.id])


