from django import forms
from .models import MatKul

class MatKulForm(forms.ModelForm):
    class Meta:
        model = MatKul
        fields = "__all__"
