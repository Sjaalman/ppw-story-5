from django.shortcuts import render, redirect
from .models import MatKul
from .forms import MatKulForm

# Create your views here.
def index(request):
    context = {
        "list_matkul" : MatKul.objects.all(),
        "page" : "index"
    }
    return render(request, 'index.html', context)

def addmatkul(request):
    if request.method == "POST":
        matkul_form = MatKulForm(request.POST)
        if matkul_form.is_valid():
            matkul_form.save()
        return redirect("story5:index")

    return render(request, 'add-matkul.html')

def delete_matkul(request,id):
    if request.method == "POST":
        matkul = MatKul.objects.get(id=id)
        matkul.delete()
    return redirect("story5:index")

def detail_matkul(request, id):
    matkul = MatKul.objects.get(id=id)
    context = {
        "matkul" : matkul,
    }
    return render(request, 'detail-matkul.html', context)

